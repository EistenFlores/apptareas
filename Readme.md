# App Tareas
Aplicacion de aprendizaje de ionic

1. clonar el proyecto

```bash
git clone git@gitlab.com:EistenFlores/apptareas.git
```

2. dirigirse a la carpeta contenedora
```bash
cd apptareas
```

3. instalar las dependencias
```bash
mpn install
```
4. hacer el biuld
```bash
ionic cordova build android --prod --release 
```

5. hacer el run del proyecto
```bash
ionic cordova run android
```