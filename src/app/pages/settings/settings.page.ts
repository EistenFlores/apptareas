import { Component, OnInit } from '@angular/core';
import { TareaService } from '../../core/services/tarea.service';
import { TareaQuery } from '../../core/querys/tarea.query';
import { TareaStore } from '../../core/store/tarea.store';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {


  constructor(
    public tareaService:TareaService,
    public tareaQuery:TareaQuery,
    private tareaStore:TareaStore) {
    // vemos los push
    this.tareaService.getPush();
    // mostramos los push
    console.log(this.tareaService.pushes);
    
   }

  ngOnInit() {
    // mostramos el token 
    this.tareaService.getToken();
  }


}
