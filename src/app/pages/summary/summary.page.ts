import { Component, OnInit } from '@angular/core';
import { TareaService } from '../../core/services/tarea.service'
import { TareaQuery } from '../../core/querys/tarea.query'
import { TareaStore } from '../../core/store/tarea.store'

@Component({
  selector: 'app-summary',
  templateUrl: './summary.page.html',
  styleUrls: ['./summary.page.scss'],
})
export class SummaryPage implements OnInit {
  
  // opciones para mostrar resumen
  opcionesResumenes=[
    "Mostrar Resumen",
    "No mostrar Resumen"
  ]
  Resumen:string;

  constructor(
    private tareaService:TareaService,
    public tareaQuery:TareaQuery,
    private tareaStore:TareaStore) {
   }

  ngOnInit() {

  }

  // moetodo para mostrar resumen
  onResumen(){
    let estado;
    console.log(this.Resumen);
    if(this.Resumen=="No mostrar Resumen"){
      estado=false;
    }else{
      estado=true;
    }
    this.tareaStore.update({
      ui: {
        showSummaryInHeader: estado
      }
    })
    
  }
}
