import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TodoPageRoutingModule } from './todo-routing.module';
import { ReactiveFormsModule } from '@angular/forms';


import { TodoPage } from './todo.page';

// agregamos el modal de agregar tarea
import { ModalAgregarTareaPage } from '../modal-agregar-tarea/modal-agregar-tarea.page';
import { ModalAgregarTareaPageModule } from '../modal-agregar-tarea/modal-agregar-tarea.module';


@NgModule({
  entryComponents:[
    ModalAgregarTareaPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TodoPageRoutingModule,
    ModalAgregarTareaPageModule,
    ReactiveFormsModule,
  ],
  declarations: [TodoPage]
})
export class TodoPageModule {}
