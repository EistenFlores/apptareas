import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TareaService } from '../../core/services/tarea.service'
import { TareaQuery } from '../../core/querys/tarea.query'
import { TareaStore } from '../../core/store/tarea.store'
import { Tarea } from '../../models/tarea.model'

// agregamos el modal de agregar tarea
import { ModalAgregarTareaPage } from '../modal-agregar-tarea/modal-agregar-tarea.page';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.page.html',
  styleUrls: ['./todo.page.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush,
})

export class TodoPage implements OnInit {
  // los estados para el select para cambiar estados
  opcionesEstados=[
    "PENDIENTE",
    "EN PROGRESO",
    "HECHO"
  ]
  // opcion seleccionada del cambiar estado
  // opcionSeleccionado: '';
  

  constructor(
    private modalController: ModalController,
    private cdr : ChangeDetectorRef,
    private tareaService:TareaService,
    public tareaQuery:TareaQuery,
    private tareStore:TareaStore) {

   }

  ngOnInit() {
    // traemos todas la tareas
    this.getTareas();
  }
  // metodo para abrir el modal de agregar tarea
  async abrirModal(){
    const modal = await this.modalController.create({
      component: ModalAgregarTareaPage,
    });

    await modal.present();
    
    // una vez cerrado el modal aca traemos la data
    const { data } = await modal.onDidDismiss();

    // dependiendo de la data hacemos el guardado
    if(data){
      console.log(data);
      // si ha sido cerrado el modal
      if(data.dismissed){
        console.log("null");
      }else{
        this.addTarea(data);
    }
    // no hay data
    }else{
      console.log("null");
    }
  }

  // capturamos la opcion del cambio de estado
  // public captura(){
  //   console.log(this.opcionSeleccionado);
  // }

  // traemos todas las tareas
  public getTareas(){
    this.tareaService.get();
  }
  
  // agregamos las tareas
  public async addTarea(formData:Tarea){
    console.log("entra a agregar");
    // creamos una tarea  
    let tarea={
      id:formData.id,
      name:formData.name,
      description:formData.description,
      dateStart:formData.dateStart,
      dateEnd:formData.dateEnd,
      state:formData.state,
    }
    // agregamos la tarea
    this.tareaService.add(tarea);
    // devolvemos la tarea creada
    console.log(this.tareStore["storeValue"]["entities"]);
  }

  // eliminamos una tarea
  eliminar(tarea){
    this.tareaService.remove(tarea.id)
  }
  
  // hacemos el cambio de estado de la tarea
  onTareaStateChange($event,tarea: Tarea) {
    const newState: string = $event.detail.value;
    this.tareaService.updateTareaState(tarea.id, newState);
  }



}
