import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormBuilder ,Validators, AbstractControl } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';


@Component({
  selector: 'app-modal-agregar-tarea',
  templateUrl: './modal-agregar-tarea.page.html',
  styleUrls: ['./modal-agregar-tarea.page.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush,
})
export class ModalAgregarTareaPage implements OnInit {
  imagen: any;
  // gets de cada atributo
  get name(){
    return this.checkoutForm.get('name');
  }

  get description(){
    return this.checkoutForm.get('description');
  }
  get dateStart(){
    return this.checkoutForm.get('dateStart');
  }
  get dateEnd(){
    return this.checkoutForm.get('dateEnd');
  }

  get state(){
    return this.checkoutForm.get('state');
  }

  // mensajes de error
  public errorMessages = {
    name : [
      {type:'required', message : 'name is required'},
      {type:'maxlength', message : 'name cant be longer than 100 characters'},
    ],
    description : [
      {type:'required', message : 'description is required'},
    ],
    dateStart : [
      {type:'required', message : 'dateStart is required'},
    ],
    dateEnd : [
      {type:'required', message : 'dateEnd is required'},
    ],
    state : [
      {type:'required', message : 'state is required'},
    ],
  }

  // creamos el checkoutForm
  checkoutForm = this.formBuilder.group({
    id : Math.random(),
    name: ['',[Validators.required,Validators.maxLength(100)]],
    description: ['',[Validators.required]],
    dateStart: ['',[Validators.required]],
    dateEnd: ['',[Validators.required, this.validateEndDate]],
    //photo: this.imagen,
    state: ['',[Validators.required]]
  });
  
  // validacion de fechas
  validateEndDate(control: AbstractControl) {
    if( control.parent ) {
      const dateEnd = control.value;
      const dateStart = control.parent.get('dateStart').value;
      if( dateEnd < dateStart ) {
        return { endDateIsInvalid: true };
      }
    }
    return null;
  }

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private camera: Camera) { 
    }

  ngOnInit() {
  }
  
  // metodo para salir
  Salir(){
    this.modalController.dismiss({
      // tendra un true cuando salga
      'dismissed': true
    }
    );
  }

  // metodo para el submit
  submit(){
    this.modalController.dismiss(
      this.checkoutForm.value
    );
  }

  // metodo para subir una foto
  subirFoto(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 300,
      targetWidth: 300,
      correctOrientation: true,
      saveToPhotoAlbum: true
    }).then(resultado=>{
      this.imagen = "data:image/jpeg;base64,"+resultado;
      console.log(this.imagen);
    }).catch(error=>{
      console.log(error);
    })
  }

}
