import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalAgregarTareaPage } from './modal-agregar-tarea.page';

const routes: Routes = [
  {
    path: '',
    component: ModalAgregarTareaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalAgregarTareaPageRoutingModule {}
