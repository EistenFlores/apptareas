import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalAgregarTareaPageRoutingModule } from './modal-agregar-tarea-routing.module';

import { ModalAgregarTareaPage } from './modal-agregar-tarea.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalAgregarTareaPageRoutingModule,
    ReactiveFormsModule 
  ],
  declarations: [ModalAgregarTareaPage]
})
export class ModalAgregarTareaPageModule {}
