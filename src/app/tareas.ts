export const tareas = [
    {
      id:1,
      name: 'Ingresar insumos',
      description: 'aca se ingresan los insumos',
      dateStart: new Date(2020,2,17),
      dateEnd: new Date(2020,3,19),
      state: "PENDIENTE"
    },
    {
      id:2,
      name: 'Procesar insumos',
      description: 'aca se procesan insumos',
      dateStart: new Date(2020,3,17),
      dateEnd: new Date(2020,3,19),
      state: "EN PROGRESO"
    },
    {
      id:3,
      name: 'Enviar a calidad',
      description: 'se ve si el producto es bueno',
      dateStart: new Date(2020,2,17),
      dateEnd: new Date(2020,2,19), 
      state: "HECHO"
    }
  ];