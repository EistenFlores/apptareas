import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { TareaStore, TareaState } from '../store/tarea.store';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class TareaQuery extends QueryEntity<TareaState> {

  // cargamos todas la tareas
  tareas$ = this.selectAll({ sortBy: 'name' });
  // el tiempo de carga
  loading$ = this.selectLoading();

  // las tareas filtradas
  tareasPendientes$ = this.tareas$.pipe(
    map( tareas => tareas.filter( tarea => tarea.state === 'PENDIENTE' ) )
  );

  tareasHechos$ = this.tareas$.pipe(
    map( tareas => tareas.filter( tarea => tarea.state === 'HECHO' ) )
  );

  tareasProgreso$ = this.tareas$.pipe(
    map( tareas => tareas.filter( tarea => tarea.state === 'EN PROGRESO' ) )
  );

  // las tareas contadas
  tareasPendientesCount$ = this.tareasPendientes$.pipe(
    map( tareas => tareas.length )
  );

  tareasHechosCount$ = this.tareasHechos$.pipe(
    map( tareas => tareas.length )
  );

  tareasProgresoCount$ = this.tareasProgreso$.pipe(
    map( tareas => tareas.length )
  );

  // para mostrar el encabezado de resumen
  showSummaryInHeader$ = this.select( state => state.ui.showSummaryInHeader );

  constructor(protected store: TareaStore) {
    super(store);
  }

}
