import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { TareaStore } from '../store/tarea.store';
import { TareaQuery } from '../querys/tarea.query';
import { Tarea } from '../../models/tarea.model';
import { mapTo } from 'rxjs/operators';
import { timer } from 'rxjs';
import { tareas } from '../../tareas'
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';



@Injectable({ providedIn: 'root' })
export class TareaService {
pushes :any = [];

  // hacemos un upddate de los estados
  updateTareaState(id: ID, newState: string) {
    this.tareaStore.update(id, currentTarea => ({ ...currentTarea, state: newState }));
    console.log(this.tareaStore["storeValue"]["entities"]);
    
  }

  constructor(
    private tareaStore: TareaStore,
    private tareaQuery: TareaQuery,
    private http: HttpClient,
    private firebaseX: FirebaseX,
    private localNotifi:LocalNotifications) {
  }

  // cremos un get para retornar las tareas
  get() {
    // con un timer
    timer(1000).pipe(mapTo(tareas))
    .subscribe(tareas=>{
      this.tareaStore.set(tareas);
      console.log(this.tareaStore["storeValue"]["entities"]);
    })
  }
  
  // con este metodo optenemos una tarea eb especifico
  getTareas(id:ID){
    if (this.tareaQuery.hasEntity(id)) {
      return this.tareaQuery.selectEntity(id);
    }
  }

  // agregamos una tarea
  add(tarea: Tarea) {
    this.tareaStore.add(tarea);
    // notificacion de una tarea agregada
    this.localNotifi.schedule({
      title:'Nueva tarea agregada',
      text:tarea.name+"\n"+tarea.description,
      trigger:{
        at:new Date(new Date().getTime()+1000)},
        led:'FF0000',
        foreground:true
    });
    this.setPush(tarea);
  }

  // actualizamos un tarea
  update(id, tarea: Partial<Tarea>) {
    this.tareaStore.update(id, tarea);
  }

  // eliminamos una tarea
  remove(id: ID) {
    this.tareaStore.remove(id);
    console.log(this.tareaStore["storeValue"]["entities"]);
  }

  // para los mensajes push para mostrarlos 
  getPush(){
    this.firebaseX.onMessageReceived().subscribe(data => {
      if (data.wasTapped) {
        console.log(data)
        console.log("Received in background");
        this.pushes.push({
          body: data.body,
          title: data.title
        })
        console.log(this.pushes)
      } else {
        console.log(data)
        console.log("Received in foreground");
        this.pushes.push({
          body: data.body,
          title: data.title
        })
        console.log(this.pushes)
      };
    });
  }

  
  setPush(tarea: Tarea){
    this.pushes.push({
      body:tarea.name+"\n"+tarea.description,
      title: 'Nueva tarea agregada'
    })
  }
  // metodo para obtener el token
  getToken(){
     console.log(this.firebaseX.getToken());
  }

}
