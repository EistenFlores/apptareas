import { Injectable } from '@angular/core';
import { Tarea } from '../../models/tarea.model';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export interface TareaState extends EntityState<Tarea> {
  ui: {
    showSummaryInHeader: boolean
  }
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'tarea' })
export class TareaStore extends EntityStore<TareaState> {

  constructor() {
    super({
      // generamos un ui el cual podra ser llamado en todas las pages
      ui: {
        showSummaryInHeader: false
      }
    });
  }

}
