import { ID } from '@datorama/akita';

// creamos el modelo de tarea
export interface Tarea {
  id: ID;
  name: string;
  description: string;
  dateStart: Date;
  dateEnd: Date;
  state: string;
}

export function createTarea(params: Partial<Tarea>) {
  return {

  } as Tarea;
}
