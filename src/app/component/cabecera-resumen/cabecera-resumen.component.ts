import { Component, OnInit } from '@angular/core';

import { TareaService } from '../../core/services/tarea.service'
import { TareaQuery } from '../../core/querys/tarea.query'

@Component({
  selector: 'app-cabecera-resumen',
  templateUrl: './cabecera-resumen.component.html',
  styleUrls: ['./cabecera-resumen.component.css']
})
export class CabeceraResumenComponent implements OnInit {

  constructor(private tareaService:TareaService,public tareaQuery:TareaQuery) { }

  ngOnInit() {
  }

}
