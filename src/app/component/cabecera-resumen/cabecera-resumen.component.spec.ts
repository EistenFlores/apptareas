import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabeceraResumenComponent } from './cabecera-resumen.component';

describe('CabeceraResumenComponent', () => {
  let component: CabeceraResumenComponent;
  let fixture: ComponentFixture<CabeceraResumenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabeceraResumenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabeceraResumenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
